# Imágenes https://www.flaticon.es/autores/freepik
# Sonidos de http://sonidosmp3gratis.com/
import pygame, sys
from pygame.locals import *
import random


ancho = 640
alto = 640
separa = 64

#-- Imágenes del juego
imagen0 = pygame.image.load('images/covid_19.png')
imagen1 = pygame.image.load('images/quedate_en_casa.png')
imagen2 = pygame.image.load('images/alcohol_en_gel.png')
imagen3 = pygame.image.load('images/barbijo.png')
imagen4 = pygame.image.load('images/lavado_manos.png')
imagenX = pygame.image.load('images/me_gusta.png')

#-- Sonidos del juego
pygame.mixer.init()
acierto = pygame.mixer.Sound('sonidos/acertar_mario.ogg')
error = pygame.mixer.Sound('sonidos/error_crash.ogg')

reloj = pygame.time.Clock()
random.seed()

modelo = []
imagenes = [imagen0, imagen1, imagen2, imagen3, imagen4]
color_bg = (128,0,128)

def main():

    #-- Inicializando la pantalla
    pantalla = pygame.display.set_mode((ancho, alto))
    pygame.display.set_caption('COVID-19 ¿Cómo Cuidarnos?')
    pantalla.fill(color_bg)
    
    cantidad_virus = 0
    
    for xm in range(0,ancho//separa):
        fila = []
        for ym in range(0,alto//separa):
            valor = random.randint(0,len(imagenes)-1)
            if valor != 0:
                cantidad_virus += 1
            fila.append(valor)
        modelo.append(fila)
    
    #-- Testing
    print(cantidad_virus)
    
    for xi in range(0,ancho//separa):
        for yi in range(0, alto//separa):
            pantalla.blit(imagenes[modelo[xi][yi]], (xi*separa, yi*separa))
            pygame.draw.rect(pantalla, (0,0,0), (xi*separa, yi*separa, separa, separa), 2)
    
    
    reloj.tick(27)
    
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                
            if event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                
                pos_x = pos[0] // separa
                pos_y = pos[1] // separa
                
                #-- Detecto si se acertó a un virus
                if modelo[pos_x][pos_y] != 0:
                    modelo[pos_x][pos_y] = None
                    acierto.play()
                    pygame.draw.rect(pantalla, (0,0,0), (pos_x*separa, pos_y*separa, separa, separa))
                    pantalla.blit(imagenX, (pos_x*separa, pos_y*separa))
                    cantidad_virus -= 1
                else:
                    error.play()
                    pygame.draw.line(pantalla, (255,0,0), (pos_x*separa, pos_y*separa), (pos_x*separa+separa, pos_y*separa+separa), 3)
                    
        
                if cantidad_virus == 0:
                    letra = pygame.font.SysFont(None, 40)
                    letra.set_bold(True)
                    img = letra.render('GANASTE', True, (0,0,255), (255,255,255))
                    pantalla.blit(img, (ancho//2, alto//2))
                    pygame.display.update()
                    pygame.time.wait(3000)
                    pygame.quit()
                    return 0
        
        pygame.display.update()
        
     
    pygame.quit()
    return 0


if __name__ == '__main__':
    pygame.init()
    main()
